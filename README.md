## Dynamic enumeration values on the JVM

This project provides an easy-to-use way to create dynamic factories for
your own enum classes.

### Background

Enumerations in Java were introduced in Java 5. At the time, the only
method of dynamically calling constructors was via reflection, and so
the JVM explicitly blocked the calling of an enum class's constructor
via reflection.

Java 7 introduced the new invokedynamic ("indy") system that uses method
handles (MH) for methods and constructors. Since the real (non-reflective)
constructor for an enum class must still be accessible (or else enum
values couldn't actually be created), you could get an MH to the enum
constructor and call that.

Further, Java 8 introduced lambdas, and added method references as the
mechanism for implementing lambdas. Method references are functional
interface implementations that directly invoke the referent of a direct
method handle (DMH) without going through the MH object, and thus are
very efficient.

Java 11 then introduced dynamic constants ("condy") which allows the
value of constants to be computed at bootstrap time. Method references
can then be created via condy instead of via indy. (`LambdaMetafactory`
doesn't currently supply a condy bootstrap method out of the box though,
so you have to write your own.)

Java 12 added constant descriptors to allow condy values to be created
from Java code, without having to generate your own bytecode.

This project uses all of the above behind the scenes to build the enum
factories. So, while you could technically use dynamic enum values as
of Java 7, the mechanisms for doing it nicely are only available as of
Java 12. 😛

### Usage

```kotlin
enum class MyEnum {
    ;

    private companion object : EnumCompanion<MyEnum> {
        override val lookup = defaultLookup
        override val factory = defaultFactory
    }

    object Factory : (String, Int) -> MyEnum by factory
}
```

You need to be able to call `MethodHandles.lookup()` inside the enum
class's main class (and in particular, not its `Companion` class), so
there's some crazy tricks to ease this process. `defaultLookup` is an
inline computed property that just calls `MethodHandles.lookup()`, and
`defaultFactory` is another inline computed property that uses the
lookup, along with the reified type of the enum class, to create the
method reference.

Note that enum values created using this library are "fake" and cannot
be used in `EnumSet` or `EnumMap` or any context where real enum values
are needed.
