package nz.cky.dynamicenum

import kotlin.test.*

class DynamicEnumTests {
    private val foo = TestEnum.Factory("foo", 31337)

    @Test
    fun `should be a real enum value`() {
        assertTrue(foo::class.java.isEnum)
    }

    @Test
    fun `should have correct name and ordinal`() {
        assertEquals("foo", foo.name)
        assertEquals(31337, foo.ordinal)
    }

    @Test
    fun `should have correct string value`() {
        assertEquals("TestEnum(name = foo, ordinal = 31337)", foo.toString())
    }
}
