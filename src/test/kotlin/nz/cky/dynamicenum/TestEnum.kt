package nz.cky.dynamicenum

enum class TestEnum {
    ;

    override fun toString() = defaultString

    private companion object : EnumCompanion<TestEnum> {
        override val lookup = defaultLookup
        override val factory = defaultFactory
    }

    object Factory : (String, Int) -> TestEnum by factory
}
