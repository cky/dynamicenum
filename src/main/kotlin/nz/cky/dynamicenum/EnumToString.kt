package nz.cky.dynamicenum

val <E : Enum<E>> E.defaultString: String
    get() = "${declaringClass.simpleName}(name = $name, ordinal = $ordinal)"
