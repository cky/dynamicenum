package nz.cky.dynamicenum

import nz.cky.dynamicenum.impl.callSiteDescFor
import java.lang.invoke.MethodHandles

interface EnumCompanion<E : Enum<E>> {
    val lookup: MethodHandles.Lookup
    val factory: EnumFactory<E>
}

inline val defaultLookup: MethodHandles.Lookup
    get() = MethodHandles.lookup()

inline val <reified E : Enum<E>> EnumCompanion<E>.defaultFactory: EnumFactory<E>
    get() = callSiteDescFor<E>().resolveConstantDesc(lookup)
