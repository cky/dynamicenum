package nz.cky.dynamicenum.impl

import nz.cky.dynamicenum.EnumFactory
import java.lang.constant.ClassDesc
import java.lang.constant.ConstantDescs
import java.lang.constant.DynamicConstantDesc
import java.lang.constant.MethodHandleDesc
import java.lang.constant.MethodTypeDesc

private val lambdaMetafactoryDesc = ConstantDescs.ofConstantBootstrap(
    ClassDesc.of("nz.cky.dynamicenum.impl.CondyLambdaMetafactory"),
    "metafactory",
    ConstantDescs.CD_Object,
    ConstantDescs.CD_MethodType,
    ConstantDescs.CD_MethodHandle,
    ConstantDescs.CD_MethodType
)

private val function2ClassDesc = ClassDesc.of("kotlin.jvm.functions.Function2")

private val function2Desc = MethodTypeDesc.of(
    ConstantDescs.CD_Object, ConstantDescs.CD_Object, ConstantDescs.CD_Object
)

@PublishedApi
internal fun <E : Enum<E>> callSiteDescFor(classDesc: ClassDesc) =
    DynamicConstantDesc.ofNamed<EnumFactory<E>>(
        lambdaMetafactoryDesc,
        "invoke",
        function2ClassDesc,
        function2Desc,
        MethodHandleDesc.ofConstructor(classDesc, ConstantDescs.CD_String, ConstantDescs.CD_int),
        MethodTypeDesc.of(classDesc, ConstantDescs.CD_String, ConstantDescs.CD_Integer)
    )

@PublishedApi
internal inline fun <reified E : Enum<E>> callSiteDescFor(): DynamicConstantDesc<EnumFactory<E>> =
    callSiteDescFor(ClassDesc.of(E::class.java.name))
