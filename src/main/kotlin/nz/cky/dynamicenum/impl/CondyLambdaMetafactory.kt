package nz.cky.dynamicenum.impl

import java.lang.invoke.LambdaMetafactory
import java.lang.invoke.MethodHandle
import java.lang.invoke.MethodHandles
import java.lang.invoke.MethodType

object CondyLambdaMetafactory {
    @JvmStatic
    fun <T> metafactory(
        lookup: MethodHandles.Lookup, name: String, type: Class<T>,
        samMethodType: MethodType, implMethod: MethodHandle,
        instantiatedMethodType: MethodType
    ): T = type.cast(
        LambdaMetafactory.metafactory(
            lookup, name, MethodType.methodType(type), samMethodType,
            implMethod, instantiatedMethodType
        ).target.invoke()
    )
}
