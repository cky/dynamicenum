package nz.cky.dynamicenum

typealias EnumFactory<E> = (name: String, ordinal: Int) -> E
