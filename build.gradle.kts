plugins {
    kotlin("jvm") version "1.4.10"
}

object Versions {
    const val KOTLIN = "1.4.10"
    const val JUNIT = "5.7.0"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib", Versions.KOTLIN))
    testImplementation(kotlin("test-junit5", Versions.KOTLIN))
    testImplementation("org.junit.jupiter", "junit-jupiter-api", Versions.JUNIT)
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", Versions.JUNIT)
}

kotlin {
    target {
        compilations {
            all {
                kotlinOptions {
                    jvmTarget = "12"
                }
            }
        }
    }
}

tasks {
    test {
        useJUnitPlatform()
    }
}
